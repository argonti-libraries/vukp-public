const topFunction = () => {
  document.body.scrollTop = 0;
  document.documentElement.scrollTop = 0;
};

//Auto resize menu
$(document).ready(function () {
  var item_width = $('#menu-desktop > li').width();
  var item_count = ($("#menu-desktop > li").length) + 1;
  var nav_width_og = $('.menu').width();
  var nav_width = $('.menu').width();

  if ((item_width * (item_count + 2)) > nav_width) {
    // $('#nav-item-more').appendTo('body');
    $('#nav-item-more').hide();
  }

  for (var i = 0; i < item_count; i++) {
    nav_width = $('.menu').width();
    item_width = $('#menu-desktop > li').width();
    item_count = ($("#menu-desktop > li").length);
    var current_width = (item_width * (item_count + 1));
    // alert(`nav_width: ${nav_width} <> current_width: ${current_width} [item_width: ${item_width} - item_count: ${item_count}]`)
    if (nav_width < current_width) {
      $('#menu-desktop > li').not('#nav-item-more').last().appendTo($('#menu-overflow'));

      // $('#nav-item-more').appendTo($('#menu-desktop'));
      $('#nav-item-more').show();
    }
  }

  $('#nav-item-more > ul > li').each(function (i, el) {
    var item = $(el);
    $(item).removeClass("dropdown nav-item").addClass("dropstart");
    $(item).find('a').attr({
      "data-bs-toggle": "dropdown"
    }).addClass('dropdown-item').removeClass('menu__item');
    $(item).find('ul').removeClass('menu__sub').addClass("dropdown-menu menu__sub_child");
  });
});

