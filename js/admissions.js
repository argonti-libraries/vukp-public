$(document).ready(function () {
  $('.feedback__content').slick({
    centerMode: true,
    slidesToShow: 3,
    cssEase: 'linear',
    variableWidth: true,
    variableHeight: false,
    accessibility: false,
    autoplaySpeed: 4000,
    autoplay:true,

    responsive: [
      {
        breakpoint: 768,
        settings: {
          arrows: false,
          centerMode: true,
          centerPadding: '20px',
          slidesToShow: 3,
        }
      },
      {
        breakpoint: 480,
        settings: {
          arrows: false,
          centerMode: true,
          centerPadding: '10px',
          slidesToShow: 1,
          arrows: true
        }
      }
    ]
  });

});
