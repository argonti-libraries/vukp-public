const scrollToSection = (id) => {
    $('html, body').animate({
        scrollTop: $(`#${id}`).offset().top
    }, 500);
}
