$(document).ready(function () {
  $('.news__content').slick({
    // centerMode: true,
    slidesToShow: 3,
    slidesToScroll: 3,
    autoplaySpeed: 3000,
    autoplay:true,
    dots: false,
    infinite: true,
    responsive: [
      {
        breakpoint: 768,
        settings: {
          arrows: false,
          centerMode: true,
          centerPadding: '20px',
          slidesToShow: 3,
        }
      },
      {
        breakpoint: 480,
        settings: {
          arrows: false,
          centerMode: true,
          centerPadding: '10px',
          slidesToShow: 1,
          arrows: true
        }
      }
    ]
  });

});
